// Libraries
import React, {Component} from 'react';
import {
    Link,
    Redirect
} from "react-router-dom";
import axios from 'axios';

// Css
import '../../public/css/auth/login.scss'

// Components
import config from '../../config';

class Login extends Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }
        this.state = {
            loggedIn: loggedIn,
            email: '',
            password: '',
            error: '',
        };
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeEmail(e) {
        this.setState({email: e.target.value});
    }

    handleChangePassword(e) {
        this.setState({password: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        let url = `${config.API_SERVER_URL}user/login`;
        axios.post(url, this.state).then((response) => {
            if (response.data.success) {
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('name', response.data.name)
                localStorage.setItem('user_id', response.data.user_id)
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;
                this.setState({loggedIn: true});
                return <Redirect to="/"/>
            } else {
                this.setState({error: response.data.message});
            }
        })
    }

    render() {
        if (this.state.loggedIn) {
            return <Redirect to="/"/>
        }
        return (
            <form id="login-form" onSubmit={this.handleSubmit} method="post">
                <h3>LOGIN </h3>
                <p name="email" className="center-block">Email<span>*</span></p>
                <input value={this.state.email} onChange={this.handleChangeEmail} id="email-input"
                       name="email" type="email" className="center-block" placeholder="Email"/>
                <p name="password" className="center-block">Password<span>*</span></p>
                <input onChange={this.handleChangePassword} id="password-input" name="password"
                       type="password" className="center-block" placeholder="Password"/>
                {this.state.error !== '' ? (<p id="error_login"> {this.state.error} </p>) : <span> </span>}
                <button onClick={this.handleSubmit}
                        className="landing-page-btn center-block text-center" id="email-login-btn" href="#facebook">
                    Login
                </button>
                <div className="group_button">
                    <p className="btn_link"><Link to="/forgot">Forgot password</Link></p>
                    <p className="btn_register"><Link to="/register">Register</Link></p>
                </div>
            </form>
        )
    }
}

export default Login
