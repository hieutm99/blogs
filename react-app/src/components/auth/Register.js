// Libraries
import React, {Component} from "react";
import axios from 'axios';
import {Link} from "react-router-dom";

// Css
import '../../public/css/auth/register.scss'

// Components
import config from '../../config';

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            confirm_password: '',
            err_name: '',
            err_email: '',
            err_password: '',
            err_confirm_password: '',
        }
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeConfirmPassword = this.handleChangeConfirmPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeName(e) {
        this.setState({name: e.target.value});
    }

    handleChangeEmail(e) {
        this.setState({email: e.target.value});
    }

    handleChangePassword(e) {
        this.setState({password: e.target.value});
    }

    handleChangeConfirmPassword(e) {
        this.setState({confirm_password: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault()
        let url = `${config.API_SERVER_URL}user/register`
        let userData = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            confirm_password: this.state.confirm_password
        }
        axios.post(url, userData).then((response) => {
            if (response.data.success) {
                alert("Register Success")
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('name', response.data.name)
                localStorage.setItem('user_id', response.data.user_id)
                this.setState({
                    err_name: '',
                    err_email: '',
                    err_password: '',
                    err_confirm_password: '',
                });
                this.props.history.push('/')
            } else {
                console.log(response.data)
                this.setState({
                    err_name: response.data.name ? response.data.name : '',
                    err_email: response.data.email ? response.data.email : '',
                    err_password: response.data.password ? response.data.password : '',
                    err_confirm_password: response.data.confirm_password ? response.data.confirm_password : '',
                });
            }
        }).catch(error => {
            console.log(error.message);
        });
    }

    render() {
        return (
            <form id="register-form" onSubmit={this.handleSubmit} method="post">
                <h3>REGISTER</h3>
                <p name="name" className="center-block">Name<span>*</span>{this.state.err_name !== '' ? (
                    <span> {this.state.err_name} </span>) : <span> </span>}</p>
                <input onChange={this.handleChangeName} id="name-input" name="name" type="text"
                       className="center-block" placeholder="Name"/>
                <p name="email" className="center-block">Email<span>*</span>{this.state.err_email !== '' ? (
                    <span> {this.state.err_email} </span>) : <span> </span>}</p>
                <input onChange={this.handleChangeEmail} id="email-input" name="email" type="email"
                       className="center-block" placeholder="Email"/>
                <p name="password" className="center-block">Password<span
                >*</span>{this.state.err_password !== '' ? (
                    <span> {this.state.err_password} </span>) : <span> </span>}</p>
                <input onChange={this.handleChangePassword} id="password-input" name="password"
                       type="password" className="center-block" placeholder="Password"/>
                <p name="confirm_password" className="center-block">Confirm
                    password<span>*</span>{this.state.err_confirm_password !== '' ? (
                        <span> {this.state.err_confirm_password} </span>) : <span> </span>}</p>
                <input onChange={this.handleChangeConfirmPassword} id="confirm-password-input"
                       name="confirm_password" type="password" className="center-block" placeholder="Confirm password"/>
                <button type="submit" className="landing-page-btn center-block text-center"
                        id="email-login-btn" href="#facebook">
                    Register
                </button>
                <p className="p_link"><Link to="/login">Login</Link></p>
            </form>
        );
    }
}

export default Register;
