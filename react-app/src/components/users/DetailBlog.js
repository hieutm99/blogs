// Libraries
import React, {Component} from 'react';
import {
    Container,
    Button
} from 'reactstrap';
import axios from 'axios';
import {Link} from "react-router-dom";

// Css
import '../../public/css/users/detail_blog.scss'

// Components
import config from '../../config';
import Page from "./Page";
import * as queryString from "query-string";

class DetailBlog extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem("token")
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }
        this.state = {
            idBlog: null,
            loggedIn: loggedIn,
            detailBlog: [],
        }
    }

    componentDidMount() {
        console.log(this.props.location.state)
        console.log('location : ', this.props.location);
        const idBlog = queryString.parse(this.props.location.search).id;
        this.setState({
            idBlog
        })
        if (idBlog) {
            // console.log('location : ', this.props.location.state.idBlog)
            let url = `${config.API_SERVER_URL}user/detail_blog?id_blog=${idBlog}`;
            axios.get(url).then(res => {
                this.setState({
                    detailBlog: res.data
                });
            });
        }
    }

    showButtonAuthor() {
        return <div><Link to={{
            pathname: '/edit_blog',
            state: {
                idBlog: this.state.idBlog,
            }
        }
        }><Button color="warning">Edit</Button></Link>
            <Button className="float-right" color="danger" onClick={() => {
                if (window.confirm('Delete the item?')) {
                    this.deleteBlog()
                }
            }}>Delete</Button></div>
    }

    deleteBlog() {
        console.log('ok ok ok')
        let url = `${config.API_SERVER_URL}user/delete_blog?id_blog=` + this.props.location.state.idBlog;
        axios.get(url).then(res => {
            if (res.data.success) {
                alert('Success')
                this.props.history.push('/')
            } else {
                alert('No')
            }
        });
    }

    render() {
        if (this.state.loggedIn === false) {
            this.props.history.push('/login')
        }
        const detailBlog = this.state.detailBlog;
        let srcImg = "http://127.0.0.1:8000/storage/uploads/" + detailBlog.image;
        return (
            <div>
                <Page/>
                <Container className="mt-2 container_detail_blog">
                    <h2>DETAIL BLOG</h2>
                    <div className="show_info_blog">
                        <img width={150} height={150} src={srcImg} alt="image"/>
                        <p><h5><b>Title:</b></h5> {detailBlog.title}</p>
                        <p><h5><b>Category:</b></h5> {detailBlog.category}</p>
                        <p><h5><b>Content:</b></h5> {detailBlog.content}</p>
                        <p><h5><b>Author:</b></h5> {detailBlog.author}</p>
                        <p><h5><b>Date:</b></h5> {detailBlog.date}</p>
                    </div>
                    {Number(localStorage.getItem('user_id')) === detailBlog.userId ? this.showButtonAuthor() :
                        <span> </span>}
                </Container>
            </div>
        )
    }
}

export default DetailBlog;
