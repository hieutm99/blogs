// Libraries
import React, {Component} from 'react';
import {
    Container,
    Form,
    FormGroup,
    Label,
    Input,
    Button,
    FormText
} from 'reactstrap';
import axios from 'axios';
import {Link} from "react-router-dom";

// Components
import config from '../../config';
import Page from "./Page";

class AddEditBlog extends Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }
        this.state = {
            loggedIn: loggedIn,
            title: '',
            category: '',
            image: '',
            content: '',
            status: false,
            err_title: '',
            err_category: '',
            err_image: '',
            err_content_w: '',
            lstCategory: [],
            editBlog: [],
        }
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeCategory = this.handleChangeCategory.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.handleChangeContent = this.handleChangeContent.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        let url1 = `${config.API_SERVER_URL}user/list_category`
        axios.get(url1).then(res => {
            this.setState({
                category: res.data[0].name,
                lstCategory: res.data
            });
        });
        if (this.props.location.state !== undefined) {
            let url2 = `${config.API_SERVER_URL}user/edit_blog?id_blog=` + this.props.location.state.idBlog;
            axios.get(url2).then(res => {
                let ckstatus = res.data.status === 1 ? true : false
                this.setState({
                    title: res.data.title,
                    category: res.data.category,
                    content: res.data.content,
                    status: ckstatus,
                    editBlog: res.data
                });
            });
        }
    }

    handleChangeTitle(e) {
        this.setState({title: e.target.value});
    }

    handleChangeCategory(e) {
        this.setState({category: e.target.value});
    }

    handleChangeImage(e) {
        this.setState({image: e.target.files[0]});
    }

    handleChangeContent(e) {
        this.setState({content: e.target.value});
    }

    handleChangeStatus(e) {
        this.setState({status: !this.state.status})
    }

    handleSubmit(e) {
        e.preventDefault()
        let url = '';
        if (this.props.location.state !== undefined) {
            url = `${config.API_SERVER_URL}user/edit_blog?id_blog=` + this.props.location.state.idBlog;
        } else {
            url = `${config.API_SERVER_URL}user/add_blog`
        }
        let ckStatus = this.state.status ? 1 : 0;
        const formData = new FormData();
        if (this.state.image !== '') {
            formData.append('image', this.state.image)
        }
        formData.append('user_id', localStorage.getItem('user_id'));
        formData.append('title', this.state.title);
        formData.append('category', this.state.category);
        formData.append('content', this.state.content);
        formData.append('status', ckStatus);
        console.log(formData)
        axios.post(url, formData).then((response) => {
            if (response.data.success) {
                alert("Success")
                this.setState({
                    err_title: '',
                    err_category: '',
                    err_image: '',
                    err_content: '',
                });
                this.props.history.push('/')
            } else {
                this.setState({
                    err_title: response.data.title ? response.data.title : '',
                    err_category: response.data.category ? response.data.category : '',
                    err_image: response.data.image ? response.data.image : '',
                    err_content: response.data.content ? response.data.content : '',
                });
            }
        }).catch(error => {
            console.log(error.message);
        });
    }

    render() {
        if (this.state.loggedIn === false) {
            this.props.history.push('/login')
        }
        const lstCategory = this.state.lstCategory;
        const editBlog = this.state.editBlog;
        return (
            <div>
                <Page/>
                <Container className="mt-2">
                    <h2>EDIT BLOG</h2>
                    <Form onSubmit={this.handleSubmit} method="post">
                        <FormGroup>
                            <Label for="title">Title</Label><span
                            style={{color: "red"}}>* {this.state.err_title !== '' ? (
                            <span style={{color: "red"}}> {this.state.err_title} </span>) : <span> </span>}</span>
                            <Input defaultValue={editBlog ? editBlog.title : this.state.title}
                                   onChange={this.handleChangeTitle} type="text"
                                   name="title"
                                   id="title" placeholder="Title"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="category">Category</Label><span
                            style={{color: "red"}}>* {this.state.err_category !== '' ? (
                            <span style={{color: "red"}}> {this.state.err_category} </span>) : <span> </span>}</span>
                            <Input defaultValue={editBlog ? editBlog.category : this.state.category}
                                   onChange={this.handleChangeCategory} type="select"
                                   name="category" id="category">
                                {lstCategory.map(itemC => (
                                    <option selected={itemC.name === editBlog.category ? true : false}
                                            value={itemC.name}>{itemC.name}</option>
                                ))}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="image">Image</Label><span
                            style={{color: "red"}}> {this.state.err_image !== '' ? (
                            <span style={{color: "red"}}> {this.state.err_image} </span>) : <span> </span>}</span>
                            <Input onChange={this.handleChangeImage} type="file" name="image" id="image"/>
                            <FormText color="muted">
                                The image must be a file of type: jpeg, jpg, png, gif.
                            </FormText>
                        </FormGroup>
                        <FormGroup>
                            <Label for="content">Content</Label><span
                            style={{color: "red"}}>* {this.state.err_content !== '' ? (
                            <span style={{color: "red"}}> {this.state.err_content} </span>) : <span> </span>}</span>
                            <Input defaultValue={editBlog ? editBlog.content : this.state.content}
                                   onChange={this.handleChangeContent} type="textarea"
                                   name="content" id="content" placeholder="Content"/>
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                                <Input checked={this.state.status} onChange={this.handleChangeStatus} id="status"
                                       name="status" type="checkbox"/>{' '}
                                <Label for="status">Is public</Label>
                            </Label>
                        </FormGroup>
                        <Button>Edit</Button>
                        <Link to="/blogs"><Button className="float-right" color="link">Back</Button></Link>
                    </Form>
                </Container>
            </div>
        )
    }
}

export default AddEditBlog;
