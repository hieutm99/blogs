// Libraries
import React, {Component} from 'react';
import {
    Link,
    withRouter
} from "react-router-dom";
import {
    Collapse,
    Nav,
    Navbar,
    NavItem,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from "reactstrap";
import axios from "axios";

class Page extends Component {
    constructor(props) {
        super(props)
        this.state = {userName: ''}
        this.logout = this.logout.bind(this)
    }

    componentDidMount() {
        if (localStorage['token']) {
            this.getProfile();
        }
    }

    getProfile() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage['token'];
        if (localStorage['name']) {
            this.setState({userName: localStorage['name']})
        }
    }

    logout() {
        /*/!*        const headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'JWT fefege...'
                }*!/
                //axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage['token'];
                let url = `${config.API_SERVER_URL}user/logout?token=` + localStorage['token'];
                axios.get(url).then((response) => {
                    if (response.data.success) {
                        this.setState({userName: ''})
                        localStorage.removeItem('token');
                        localStorage.removeItem('name');
                        localStorage.removeItem('user_id');
                        this.props.history.push('/login');
                    }else{
                        console.log(response.data.message)
                    }
                }).catch(error => {
                    console.log("ERRRR:: ",error.response.data);
                })*/
        this.setState({userName: ''})
        localStorage.removeItem('token');
        localStorage.removeItem('name');
        localStorage.removeItem('user_id');
        this.props.history.push('/login');
    }

    render() {
        return (
            <div>
                <Navbar color="dark" light expand="md">
                    <Collapse navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem className="p-2">
                                <Link style={styles.navItem} to="/blogs">List blogs</Link>
                            </NavItem>
                            <NavItem className="p-2">
                                <Link style={styles.navItem} to="/add_blog">Add blog</Link>
                            </NavItem>
                        </Nav>
                        <Nav className="ml-auto" navbar>
                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret>
                                    <span style={styles.navItem}>Welcome {this.state.userName}</span>
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem onClick={this.logout}>
                                        Log out
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

const styles = {
    navItem: {
        color: "white",
    }
}
export default withRouter(Page)
