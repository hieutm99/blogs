// Libraries
import React, {Component} from 'react';
import {
    Container,
    Form,
    FormGroup,
    Label,
    Input,
    Button,
} from 'reactstrap';
import axios from 'axios';

// Components
import config from '../../config';

class ResetPassword extends Component {
    constructor (props) {
        super(props)
        this.state = {
            email: '',
            verify_code: '',
            password: '',
            confirm_password: '',
            err_email: '',
            err_verify_code: '',
            err_password: '',
            err_confirm_password: '',
        }
        this.handleChangeVerifyCode = this.handleChangeVerifyCode.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeConfirmPassword = this.handleChangeConfirmPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeVerifyCode (e) {
        this.setState({verify_code: e.target.value});
    }

    handleChangePassword (e) {
        this.setState({password: e.target.value});
    }

    handleChangeConfirmPassword (e) {
        this.setState({confirm_password: e.target.value});
    }

    handleSubmit (e) {
        e.preventDefault()
        let url = `${config.API_SERVER_URL}reset-password`
        let userData = {
            verify_code: this.state.verify_code,
            password: this.state.password,
            confirm_password: this.state.confirm_password
        }
        axios.put(url, userData).then((response) => {
            if (response.data.success) {
                alert("Reset Password Success")
                this.setState({
                    err_verify_code: '',
                    err_password: '',
                    err_confirm_password: '',
                });
                this.props.history.push('/login')
            } else {
                this.setState({
                    err_verify_code: response.data.verify_code ? response.data.verify_code : '',
                    err_password: response.data.password ? response.data.password : '',
                    err_confirm_password: response.data.confirm_password ? response.data.confirm_password : '',
                });
                alert(response.data.message)
            }
        }).catch(error => {
            console.log(error.message);
        });
    }

    render() {
        if (this.props.location.state === undefined){
            this.props.history.push('/login')
        }
        return (
            <Container className="mt-2">
                <h2 style={{textAlign: "center"}}>RESET PASSWORD</h2>
                <Form onSubmit={this.handleSubmit} method="post" >
                    <FormGroup>
                        <Label for="email">Email</Label><span style={{color: "red"}}>* </span>
                        <Input disabled value={this.props.location.state.email} type="email" name="email" id="email" placeholder="Email" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="verify_code">Verify code</Label><span style={{color: "red"}}>* {this.state.err_verify_code !== '' ? (<span style={{color: "red"}}> {this.state.err_verify_code} </span>) : <span> </span>}</span>
                        <Input onChange={this.handleChangeVerifyCode} type="text" name="verify_code" id="verify_code" placeholder="Verify code" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">New password</Label><span style={{color: "red"}}>* {this.state.err_password !== '' ? (<span style={{color: "red"}}> {this.state.err_password} </span>) : <span> </span>}</span>
                        <Input onChange={this.handleChangePassword} type="password" name="password" id="password" placeholder="Password" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="confirm_password">Renew password</Label><span style={{color: "red"}}>* {this.state.err_confirm_password !== '' ? (<span style={{color: "red"}}> {this.state.err_confirm_password} </span>) : <span> </span>}</span>
                        <Input onChange={this.handleChangeConfirmPassword} type="password" name="confirm_password" id="confirm_password" placeholder="Renew password" />
                    </FormGroup>
                    <Button>Reset</Button>
                </Form>
            </Container>
        )
    }
}

export default ResetPassword;
