// Libraries
import React, {Component} from 'react';
import {
    Container,
    Form,
    FormGroup,
    Label,
    Input,
    Button,
} from 'reactstrap';
import axios from 'axios';

// Components
import config from '../../config';

class ForgotPassword extends Component {
    constructor (props) {
        super(props)
        this.state = {
            email: '',
            err_email: '',
        }
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeEmail (e) {
        this.setState({email: e.target.value});
    }

    handleSubmit (e) {
        e.preventDefault()
        let url = `${config.API_SERVER_URL}reset-password`
        let userData = {
            email: this.state.email,
        }
        axios.post(url, userData).then((response) => {
            if (response.data.success) {
                alert("Forgot Password Success")
                this.setState({
                    err_email: '',
                });
                this.props.history.push({
                    pathname: '/reset',
                    state: { email: response.data.email }
                })
            } else {
                this.setState({
                    err_email: response.data.email ? response.data.email : '',
                });
            }
        }).catch(error => {
            console.log(error.message);
        });
    }

    render() {
        return (
            <Container className="mt-2">
                <h2 style={{textAlign: "center"}}>FORGOT PASSWORD</h2>
                <Form onSubmit={this.handleSubmit} method="post" >
                    <FormGroup>
                        <Label for="email">Email</Label><span style={{color: "red"}}>* {this.state.err_email !== '' ? (<span style={{color: "red"}}> {this.state.err_email} </span>) : <span> </span>}</span>
                        <Input onChange={this.handleChangeEmail} type="email" name="email" id="email" placeholder="Email" />
                    </FormGroup>
                    <Button>Submit</Button>
                </Form>
            </Container>
        )
    }
}

export default ForgotPassword;
