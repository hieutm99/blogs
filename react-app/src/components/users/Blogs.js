// Libraries
import React, {Component} from 'react';
import {
    Container,
    Table,
    Form,
    FormGroup,
    Input,
    Button
} from 'reactstrap';
import axios from 'axios';
import {Link} from "react-router-dom";

// Components
import config from '../../config';
import Page from "./Page";

class Blogs extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem("token")
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }
        this.state = {
            loggedIn: loggedIn,
            queryKeyword: "",
            queryAuthor: "",
            lstBlogs: [],
            filteredData: []
        }
        this.handleInputKeywordChange = this.handleInputKeywordChange.bind(this);
        this.handleInputAuthorChange = this.handleInputAuthorChange.bind(this);
        this.onClickSearch = this.onClickSearch.bind(this);
    }

    componentDidMount() {
        let url = `${config.API_SERVER_URL}user/blog?id=` + localStorage.getItem('user_id');
        axios.get(url).then(res => {
            const {queryAuthor} = this.state;
            const filteredData = res.data.filter(element => {
                return element.author.toLowerCase().includes(queryAuthor.toLowerCase());
            });
            let lstBlogs = res.data
            this.setState({
                lstBlogs,
                filteredData
            });
        });
    }

    handleInputKeywordChange(e) {
        this.setState({queryKeyword: e.target.value});
    };

    handleInputAuthorChange(e) {
        this.setState({queryAuthor: e.target.value});
    };

    onClickSearch() {
        const queryAuthor = this.state.queryAuthor.trim();
        const queryKeyword = this.state.queryKeyword.trim();
        this.setState(prevState => {
            let filteredData = prevState.lstBlogs.filter(element => {
                return element.author.toLowerCase().includes(queryAuthor.toLowerCase());
            });
            return {
                queryAuthor,
                filteredData
            };
        });
        this.setState(prevState => {
            const filteredData = prevState.filteredData.filter(element => {
                return element.content.toLowerCase().includes(queryKeyword.toLowerCase());
            });
            return {
                queryKeyword,
                filteredData
            };
        });
        this.setState(prevState => {
            const filteredData = prevState.filteredData.filter(element => {
                return element.title.toLowerCase().includes(queryKeyword.toLowerCase());
            });
            return {
                queryKeyword,
                filteredData
            };
        });
    }

    render() {
        if (this.state.loggedIn === false) {
            this.props.history.push('/login')
        }
        const lstBlogs = this.state.filteredData;
        return (
            <div>
                <Page/>
                <Container className="mt-2">
                    <h2>LIST BLOGS</h2>
                    <Form inline>
                        <FormGroup>
                            <Input value={this.state.queryKeyword} onChange={this.handleInputKeywordChange} type="text"
                                   name="inputKeyword" id="inputKeyword" placeholder="Keyword (title, content)"/>
                        </FormGroup>
                        <FormGroup>
                            <Input value={this.state.queryAuthor} onChange={this.handleInputAuthorChange} type="text"
                                   name="inputAuthor" id="inputAuthor" placeholder="Author name"/>
                        </FormGroup>
                        <Button onClick={this.onClickSearch}>Search</Button>
                    </Form>
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Content</th>
                            <th>Author</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        {lstBlogs.map((itemB, index) => (
                            <tr key={index}>
                                <th scope="row">{index + 1}</th>
                                <td><Link to={{
                                    pathname: '/detail_blog',
                                    search: `?id=${itemB.id}`,
                                    state: {
                                        idBlog: itemB.id,
                                    }
                                }
                                }>{itemB.title}</Link></td>
                                <td>{itemB.category}</td>
                                <td>{itemB.content.length >= 50 ? itemB.content.substring(0, 50) + '...' : itemB.content}</td>
                                <td>{itemB.author}</td>
                                <td>{itemB.date}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}

export default Blogs;
