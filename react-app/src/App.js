// Libraries
import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import history from "./history";

// Css
import './App.css';

// Components
import Login from './components/auth/Login'
import Register from './components/auth/Register'
import Blogs from "./components/users/Blogs";
import DetailBlog from "./components/users/DetailBlog";
import AddEditBlog from "./components/users/AddEditBlog";
import ForgotPassword from "./components/users/ForgotPassword";
import ResetPassword from "./components/users/ResetPassword";

class App extends Component {
    render() {
        return (
            <Router history={history}>
                <div className="App">
                    <Container style={{textAlign: "left"}}>
                        <Row>
                            <Col>
                                <Switch>
                                    <Route path='/login' exact>
                                        <Login />
                                    </Route>
                                    <Route path='/register' exact component={Register}/>
                                    <Route path='/forgot' exact component={ForgotPassword}/>
                                    <Route path='/reset' exact component={ResetPassword}/>
                                    <Route path='/' exact component={Blogs}/>
                                    <Route path='/blogs' exact component={Blogs}/>
                                    <Route path='/detail_blog' exact component={DetailBlog}/>
                                    <Route path='/edit_blog' exact component={AddEditBlog}/>
                                    <Route path='/add_blog' exact component={AddEditBlog} />
                                </Switch>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </Router>
        );
    }
}

export default App;
