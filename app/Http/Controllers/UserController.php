<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use JWTAuthException;
use Validator;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5|max:25',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|max:25',
            'confirm_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $payload = [
            'password' => \Hash::make($request->password),
            'email' => $request->email,
            'name' => $request->name,
            'auth_token' => ''
        ];
        $user = new User($payload);
        if ($user->save()) {
            $credentials = $request->only('email', 'password');
            if (!($token = JWTAuth::attempt($credentials))) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], Response::HTTP_BAD_REQUEST);
            }
            $user = User::where('email', $request->email)->first();
            $user->auth_token = $token;
            $user->save();
            Mail::send('register_success', array('userId' => $user->id, 'name' => $user->name), function ($message) use ($user) {
                $message->to($user->email, 'Visitor')->subject('Visitor Feedback!');
            });
            $response = ['success' => true, 'token' => $token, 'name' => $user->name, 'user_id' => $user->id];
            return response()->json($response, Response::HTTP_OK);
        } else {
            $response = ['success' => false, 'message' => 'Register Failed'];
            return response()->json($response, Response::HTTP_OK);
        }

    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:8|max:25',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user = User::where('email', $request->email)->get()->first();
        if ($user && \Hash::check($request->password, $user->password)) {
            $credentials = $request->only('email', 'password');
            if (!($token = JWTAuth::attempt($credentials))) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], Response::HTTP_BAD_REQUEST);
            }
            $user->auth_token = $token;
            $user->save();
            $response = ['success' => true, 'token' => $user->auth_token, 'name' => $user->name, 'user_id' => $user->id];
        } else {
            $response = ['success' => false, 'message' => 'User doesnt exist'];
        }
        return response()->json($response, Response::HTTP_OK);
    }

    /*public function logout(Request $request)
    {
        $token = $request->header('Authorization');
        JWTAuth::parseToken()->invalidate($token);
    }*/
}
