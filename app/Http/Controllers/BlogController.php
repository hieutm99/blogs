<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blogs;
use App\Models\Categorys;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Validator;

class BlogController extends Controller
{
    protected $modelBlog;

    public function __construct(Blogs $modelBlog)
    {
        $this->modelBlog = $modelBlog;
    }

    public function addOrUpdateBlog(Request $request)
    {
        if ($request->id_blog) {
            $valImage = 'nullable|mimes:jpeg,jpg,png,gif|max:2028';
        } else {
            $valImage = 'required|mimes:jpeg,jpg,png,gif|max:2028';
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|max:100',
            'category' => 'required',
            'image' => $valImage,
            'content' => 'required|min:3|max:1000'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        if ($request->id_blog) {
            $blog = Blogs::find($request->id_blog);
        } else {
            $blog = new Blogs;
        }
        if ($request->hasFile('image')) {
            $uniqueid = uniqid();
            $extension = $request->file('image')->getClientOriginalExtension();
            $name = $uniqueid . '.' . $extension;
            $request->file('image')->storeAs('public/uploads', $name);

            $blog->image = $name;
        }
        $blog->title = $request->title;
        $blog->category = $request->category;
        $blog->content = $request->content;
        $blog->status = $request->status;
        $blog->user_id = $request->user_id;
        if ($blog->save()) {
            $response = ['success' => true];
        } else {
            $response = ['success' => false, 'title' => $blog->title, 'category' => $blog->category, 'image' => $blog->image, 'content' => $blog->content];
        }
        return response()->json($response, 200);
    }

    public function listBlog(Request $request)
    {
        $lstBlog = $this->modelBlog->getListBlog($request->id);
        return response()->json($lstBlog, 200);
    }

    public function listCategory()
    {
        $lstCategory = Categorys::all();
        return response()->json($lstCategory, 200);
    }

    public function detailBlog(Request $request)
    {
        $blog = $this->modelBlog->getDetailBlog($request->id_blog);
        return response()->json($blog, 200);
    }

    public function editBlog(Request $request)
    {
        $blog = Blogs::find($request->id_blog);
        return response()->json($blog, 200);
    }

    public function deleteBlog(Request $request)
    {
        $delBlog = Blogs::find($request->id_blog);
        if ($delBlog->delete()) {
            $response = ['success' => true];
        } else {
            $response = ['success' => false];
        }
        return response()->json($response, 200);
    }
}
