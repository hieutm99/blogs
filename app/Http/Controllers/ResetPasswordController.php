<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Notifications\ResetPasswordRequest;
use Validator;

class ResetPasswordController extends Controller
{
    public function sendMail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user = User::where('email', $request->email)->firstOrFail();
        $passwordReset = PasswordReset::updateOrCreate([
            'email' => $user->email,
        ], [
            'token' => Str::random(8),
        ]);
        if ($passwordReset) {
            $user->notify(new ResetPasswordRequest($passwordReset->token));
        }
        $response = ['success' => true, 'email' => $passwordReset->email];
        return response()->json($response);
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verify_code' => 'required|exists:password_resets,token',
            'password' => 'required|min:8|max:25',
            'confirm_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $passwordReset = PasswordReset::where('token', $request->verify_code)->firstOrFail();
        if ($passwordReset) {
            if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
                $passwordReset->delete();
                return response()->json([
                    'message' => 'This password reset token is invalid.',
                ], 422);
            }
            $user = User::where('email', $passwordReset->email)->firstOrFail();
            if ($user) {
                $user->update(array('password' => \Hash::make($request->password)));
            } else {
                return response()->json(['success' => false, 'message' => 'User doesnt exist']);
            }
            $passwordReset->delete();
        } else {
            return response()->json(['success' => false, 'message' => 'Token doesnt exist']);
        }
        $response = ['success' => true];
        return response()->json($response);
    }
}
