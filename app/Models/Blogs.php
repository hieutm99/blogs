<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'category', 'image', 'content', 'status', 'user_id'
    ];

    public function users()
    {
        return $this->hasMany('App\Users');
    }

    public function getDetailBlog($idBlog) {
        $detailBlog = Blogs::join('users', 'blogs.user_id', '=', 'users.id')
            ->select('blogs.id', 'blogs.title', 'blogs.category', 'blogs.image', 'blogs.content', 'users.name as author', 'blogs.updated_at as date', 'blogs.user_id as userId')
            ->where('blogs.id', '=', $idBlog)
            ->first();
        return $detailBlog;
    }

    public function getListBlog($idUser){
        $listBlogByUser = Blogs::join('users', 'blogs.user_id', '=', 'users.id')
            ->select('blogs.id', 'blogs.title', 'blogs.category', 'blogs.content', 'users.name as author', 'blogs.updated_at as date')
            ->where('blogs.user_id', '=', $idUser);
        $listBlogByOtherUser = Blogs::join('users', 'blogs.user_id', '=', 'users.id')
            ->select('blogs.id', 'blogs.title', 'blogs.category', 'blogs.content', 'users.name as author', 'blogs.updated_at as date')
            ->where('blogs.user_id', '<>', $idUser)
            ->where('blogs.status', '=', 1)
            ->union($listBlogByUser)
            ->get();
        return $listBlogByOtherUser;
    }

}
