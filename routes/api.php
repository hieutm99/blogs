<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/login', 'UserController@login');
Route::post('user/register', 'UserController@register');

Route::post('reset-password', 'ResetPasswordController@sendMail');
Route::put('reset-password', 'ResetPasswordController@reset');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user/logout', 'UserController@logout');
    Route::post('user/add_blog', 'BlogController@addOrUpdateBlog');
    Route::get('user/blog', 'BlogController@listBlog');
    Route::get('user/list_category', 'BlogController@listCategory');
    Route::get('user/detail_blog', 'BlogController@detailBlog');
    Route::get('user/edit_blog', 'BlogController@editBlog');
    Route::post('user/edit_blog', 'BlogController@addOrUpdateBlog');
    Route::get('user/delete_blog', 'BlogController@deleteBlog');
});
